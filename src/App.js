import logo from './logo.svg';
import './App.css';
import { Route, Routes} from 'react-router-dom';
import Dashboard from './projcomponent/Dashboard';
import Login from './projcomponent/auth/login';
import Register from './projcomponent/auth/register';

function App() {
  return (
    <div className="App">
      
    <Routes>
      <Route path='/' element={<Login/>}/>
      <Route path='/registation' element={<Register/>}/>
     <Route path='/dashboard' element={<Dashboard/>}/>
    </Routes>
    </div>
  );
}

export default App;
