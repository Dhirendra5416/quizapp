import React from 'react'
import { Corasal } from '../components/molecules/Corasal'
import { Footer } from '../components/organism/Footer'
import Navbar from '../components/organism/Navbar'

const Layout = ({ children }) => {
    return (
        <>
            <div >
                <Navbar />
            </div>
          
            <div className=' overflow-y-auto  bg-gray-200'>
                <div className='container mx-auto mt-4'>
                    {children}
                </div>

            </div>
            <div>
                <Footer />


            </div>
        </>
    )
}

export default Layout