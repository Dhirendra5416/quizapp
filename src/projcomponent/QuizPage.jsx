import React from 'react'

const QuizPage = () => {
  return (
    <div className='bg-gray-300 h-screen w-screen'>

    <div className='flex flex-wrap gap-6 justify-between'>

        <div className='bg-white shadow-2xl rounded-md h-auto w-auto'>
            <div className='px-8 py-4'>
                <div className=''>
                    <h1> Q.1 Who is Prime Minister of Japan?</h1>

                    <div>
                        <div className='space-x-2'>
                            <span>1</span>

                            <input type={'checkbox'} defaultChecked={checked}
                                onChange={() => setChecked(!checked)} />
                            <label>Dhrendra</label>
                        </div>
                        <div className='space-x-2'>
                            <span>2</span>
                            <input type={'checkbox'} />
                            <label>Dhrendra</label>
                        </div>
                        <div className='space-x-2'>
                            <span>3</span>
                            <input type={'checkbox'} />
                            <label>Dhrendra</label>
                        </div>
                        <div className='space-x-2'>
                            <span>4</span>
                            <input type={'checkbox'} />
                            <label>Dhrendra</label>
                        </div>
                    </div>


                </div>
            </div>
        </div>



    </div>

</div>
  )
}

export default QuizPage