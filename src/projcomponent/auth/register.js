import React, {useState}from 'react'
import { Button, InputField } from '../../components/atom/Input-fields';

const Register = () => {
  const [data, setData] = useState({
    email: '',
    password: '',
    keep_login: false,
});


const handleChange = (e) => {
    setData(() => {
        data[e.target.name] =
            e.target.name ===  e.target.value;
        return data;
      
    });
    console.log(data)
}
  return (
    <div className='bg-gray-300 w-screen h-screen'>
      
        <div className='container mx-auto my-8 inline-flex justify-center'>
        <form className='space-y-4 items-center bg-white max-w-md p-16' >
          <h1 className='font-semibold text-center text-lg'>Registeration Form for Quiz</h1>
                    <InputField type={'text'} id={'name'} label={'Name'} handleChange={handleChange} />
                    <InputField type={'email'} id={'email'} label={'Email'} handleChange={handleChange} />
                    <InputField type={'number'} id={'name'} label={'Name'} handleChange={handleChange} />
                    <InputField type={'password'} id={'password'} label={'Password'} handleChange={handleChange} />
                    <div className='flex justify-around'>
                    <Button label={'Save'}/>
                    <Button label={'LogIn'}/>
                    </div>
                   
                </form>
        </div>
    </div>
  )
}

export default Register;