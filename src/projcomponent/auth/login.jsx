import React, { useState } from 'react'
import { Button, InputField } from '../../components/atom/Input-fields';

const Login = () => {
    const [data, setData] = useState({
        email: '',
        password: '',
        keep_login: false,
    });


    const handleChange = (e) => {
        setData(() => {
            data[e.target.name] =
                e.target.name === 'keep_login' ? !data.keep_login : e.target.value;
            return data;

        });
        console.log(data)
    }
    return (
        <div className="flex flex-row w-full h-screen"
            style={{
                backgroundSize: 'cover',
                backgroundAttachment: 'fixed',
                backgroundImage: 'url(images/dhirendra.jpg)',
            }}>

            <div className='container mx-auto my-8 inline-flex justify-center'>
                <form className='space-y-4 items-center bg-white max-w-md p-16 max-h-lg' >

                    <InputField type={'text'} id={'name'} label={'Name'} handleChange={handleChange} />
                    <InputField type={'email'} id={'email'} label={'Email'} handleChange={handleChange} />
                    <InputField type={'password'} id={'password'} label={'Password'} handleChange={handleChange} />
                    <div className='flex justify-around'>
                        <Button label={'LogIn'}/>
                        <Button label={'SignUp'}/>
                    </div>
                </form>
            </div>
        </div>

        
    
)
    }

export default Login;