
import React, { useState } from 'react'
import { Card } from '../components/molecules/card'
import Layout from './Layout'





const Dashboard = () => {
   
    return (
    <Layout>
        <div className='flex flex-wrap gap-6  justify-around py-20 '>
            
            <Card quiztype={'Science'} image={"images/23-08.jpg"}/>
          
         
           <Card quiztype={'Geography'} image={"images/geography.jpg"}/>
           <Card quiztype={'GK'} image={"/images/gk.jpg"}/>
           <Card quiztype={'HTML'}image={"images/html.png"}/>
           <Card quiztype={'CSS'} image={"images/css3.png"}/>
           <Card quiztype={'JavaScript'} image={"images/javascript.png"}/>
          

        </div>
       
    </Layout>
    )
}

export default Dashboard