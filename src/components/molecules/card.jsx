import React from 'react'

export const Card = ({ quiztype, image }) => {
    return (

        <div class="w-full max-w-sm  bg-white rounded-xl border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 ">

            <div class="flex flex-col items-center pb-6">
                <img class="mb-3 w-20 h-20 rounded-full shadow-lg" src={image} alt="science" />
                <h5 class="mb-1 text-xl font-medium text-gray-900 dark:text-white">{quiztype}</h5>
                <span class="text-sm text-gray-500 dark:text-gray-400">Rank</span>
                <div class="flex mt-4 space-x-3 md:mt-6">
                    <a href="#" class="inline-flex items-center py-2 px-4 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800">Score</a>
                    <a href="#" class="inline-flex items-center py-2 px-4 text-sm font-medium text-center text-gray-900 bg-white rounded-lg border border-gray-300 hover:bg-green-600">Play</a>
                </div>
            </div>
        </div>

    )
}
