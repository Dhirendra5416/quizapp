import React from 'react'
import { SearchIcon } from './icon'

export const Searchbar = () => {
    return (
        <div className='bg-white w-auto p-1 h-auto rounded-lg border focus:border-blue-500'>
            <div className='flex flex-wrap  justify-center'>
                <div className='mt-3'><SearchIcon /></div>

                <input type={"search"} className="w-auto h-auto appearance-none focus-visible:outline-none" />
                <Button />

            </div>

        </div>
    )
}




export const Button = ({label}) => {
    return (
        <div>

            <button type="submit" className="bg-blue-500 px-6 py-2 rounded-lg">{label}</button>
        </div>
    )
}


export const InputField = ({ label,id,type,handleChange }) => {
    return (
        <div className="relative">
            <input type={type} id={id} className="block rounded-lg px-2.5 pb-1.5 pt-4 w-full text-sm text-gray-900 bg-gray-50 dark:bg-gray-700 border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " onChange={handleChange}/>
            <label for="small_filled" className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-3 scale-75 top-3 z-10 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-3">{label}</label>
        </div>
    )
}