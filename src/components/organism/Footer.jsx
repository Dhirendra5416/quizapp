import React from 'react'
import { Link } from 'react-router-dom'


const classes ={
  text1:"text-white text-lg font-semibold",
  text2:'text-gray hover:text-white hover:bg-blue-300 text-center text-sm font-normal'
}


export const Footer = () => {
 
  return (
    <div className=' bg-blue-400 h-80 w-auto'>
        <div className='container mx-auto pt-16'>
           <div className='flex flex-wrap justify-around'>
            <div className='grid items-center justify-around space-y-4'>
              <h1 className={classes.text1}>Our Products</h1>
              <div className={classes.text2}>Games</div>
              <div className={classes.text2}>Web Development</div>
              <div className={classes.text2}>Android Application</div>
            
            </div>
            <div className='grid items-center justify-around space-y-4'>
              <h1 className={classes.text1}>Our Company</h1>
              <Link> <div className={classes.text2}>About</div></Link>
              <Link> <div className={classes.text2}>Contact</div></Link>
              <Link> <div className={classes.text2}>Terms & Condition</div></Link>
           
            
            </div>
            <div className='grid items-center justify-around space-y-4'>
              <h1 className={classes.text1}>Find US</h1>
              <div className={classes.text2}>Instagram</div>
              <div className={classes.text2}>Facebook</div>
              <div className={classes.text2}>LinkedIn</div>
            
            </div>
           
            
           </div>
        </div>

</div>
  )
}
