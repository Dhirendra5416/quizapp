import React from 'react'
import { Searchbar } from '../atom/Input-fields'

const Navbar = () => {
    return (
        <div className=' fixed z-10 bg-blue-400 h-16 w-screen shadow-lg'>
            <div className='container mx-auto px-4  pt-4'>
                <div className='flex  '>

                 <>
                    <div className='bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 px-4 py-2 shadow-xl rounded-lg text-white'>TechoQuiz</div>
                 </>
                    <div className='flex-1' />


                    <div className='flex gap-4 pr-4'>
                        <div className='w-4 h-4 rounded-full'>

                            <img src="/images/dhirendra.jpg" alt='Profile' className=' object-cover' />
                        </div>
                        <div>Dhirendra</div>
                    </div>

                </div>

            </div>
        </div>
    )
}

export default Navbar